package hello;

import java.awt.Container;
import java.awt.dnd.DropTarget;

import javax.swing.JFrame;
import javax.swing.JTextArea;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public MainFrame() {
		this.setSize(480, 300);
		MainFrame frm = this;
		Container p = frm.getContentPane();
		
		JTextArea txt = new JTextArea();
		txt.setSize(this.getSize());
		p.add(txt);
		
			
		VDropTargetAdapter ddListener = new VDropTargetAdapter();
		ddListener.text = txt;
		new DropTarget(txt,ddListener);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}

    
}
