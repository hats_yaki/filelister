package hello;

import java.io.File;
import java.util.List;

public class VFileInfo {
	public String name;
	public String path;
	public double size;
	public File file;
	public List<VFileInfo> childFiles;
	public Integer fileCnt;

	public String toString() {
		StringBuffer strBuf = new StringBuffer();
		double sizeM = (int) (10 * size / (1024 * 1024)) / 10;
		strBuf.append(file.isFile() ? "F" : "D");
		strBuf.append("\t");
		strBuf.append(fileCnt);
		strBuf.append("\t");
		strBuf.append(sizeM + "MB");
		strBuf.append("\t");
		strBuf.append(name);
		return strBuf.toString();
	}

}
