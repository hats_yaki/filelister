package hello;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Util {
	
	public static void out(String str){
		System.out.println(str);
	}
	
	public static List<VFileInfo> getFileList(String path){
		List<VFileInfo> rtnVal = new ArrayList<VFileInfo>();
		
		File f = new File(path);		
		File[] list = f.listFiles();
		for (File file : list) {
			VFileInfo v = new VFileInfo();
			v.name = file.getName();
			v.path = file.getPath();
			v.file = file;
			if(file.isDirectory()){
				List<VFileInfo> subList = getFileList(file.getPath());
				double sizeM = 0;
				Integer fileCnt = 0;
				for (VFileInfo vFileInfo : subList) {
					sizeM += vFileInfo.size;
					fileCnt += vFileInfo.fileCnt;
				}
				v.size = sizeM;
				v.childFiles = subList;
				v.fileCnt = fileCnt;
			}else{
				v.size = file.length();
				v.fileCnt = 1;
			}
			rtnVal.add(v);
		}
		
		return rtnVal;
	}
	
}
