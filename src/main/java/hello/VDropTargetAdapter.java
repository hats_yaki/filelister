package hello;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.util.List;

import javax.swing.JTextArea;

public class VDropTargetAdapter extends DropTargetAdapter {
	public JTextArea text;

	public void drop(DropTargetDropEvent e) {
		try {
			Transferable transfer = e.getTransferable();
			// ファイルリストの転送を受け付ける
			if (transfer.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
				// copyとmoveを受け付ける
				e.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
				// ドラッグ＆ドロップされたファイルのリストを取得
				@SuppressWarnings("unchecked")
				List<File> fileList = (List<File>) transfer
						.getTransferData(DataFlavor.javaFileListFlavor);
				// 取得したファイルの名称を表示
				for (File file : fileList) {
					// Util.out("" + file.getName() + " " + file.getPath());
					List<VFileInfo> list = Util.getFileList(file.getPath());
					StringBuffer msg = new StringBuffer();
					msg.append(file.getPath());
					msg.append("\n");
					for (VFileInfo vFileInfo : list) {
						msg.append(vFileInfo.toString());
						msg.append("\n");
					}
					if (text != null) {
						text.setText(msg.toString());
					}
					Util.out(msg.toString());

					break;
				}

			}
		} catch (Exception err) {
			err.printStackTrace();
		}
	}
}
